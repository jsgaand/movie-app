package com.juan.testdacodes

class Config {
    companion object {
        const val BASE_URL:String ="https://api.themoviedb.org/3/"
        const val BASE_URL_IMAGES:String = "https://image.tmdb.org/t/p/w500"
        const val API_KEY:String = "a28c4bc831b590dc669ef8a459fdbff7"
    }
}