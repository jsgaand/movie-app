package com.juan.testdacodes.rest

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.juan.testdacodes.Config
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

public class TestDaCodesClient {

    companion object {
        private const val DEFUALT_TIME_OUT: Long = 60
        private const val DEFAULT_TIME_OUT_WRITE: Long = 45
        private const val DEFAULT_TIME_OUT_READ: Long = 45

        private var retrofit: Retrofit? = null

        fun getClient():Retrofit {
            if (retrofit == null){
                val okHttpClient:OkHttpClient = OkHttpClient().newBuilder()
                    .connectTimeout(DEFUALT_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(DEFAULT_TIME_OUT_WRITE, TimeUnit.SECONDS)
                    .readTimeout(DEFAULT_TIME_OUT_READ, TimeUnit.SECONDS)
                    .build()

                val gson:Gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create()

                retrofit = Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build()
            }
            return retrofit!!
        }
    }
}