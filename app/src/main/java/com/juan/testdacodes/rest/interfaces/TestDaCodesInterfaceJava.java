package com.juan.testdacodes.rest.interfaces;

import com.juan.testdacodes.mvp.models.BaseResponse;
import com.juan.testdacodes.mvp.models.MovieDetailResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TestDaCodesInterfaceJava {

    @Headers({"Accept: application/json"})
    @GET("movie/now_playing")
    Call<BaseResponse> getMovies(
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @Headers({"Accept: application/json"})
    @GET("movie/{movie_id}")
    Call<MovieDetailResponse> getMovieDetails(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey
    );

}
