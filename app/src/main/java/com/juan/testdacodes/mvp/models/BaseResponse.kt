package com.juan.testdacodes.mvp.models

import com.google.gson.annotations.SerializedName

data class BaseResponse(

    var page: String,
    var results: ArrayList<Result>,
    var dates: Date,
    @SerializedName("total_pages")
    var totalPages:Int,
    @SerializedName("total_results")
    var totalResults:Int,

    //Errores
    @SerializedName("status_message")
    var statusMessage:String,
    @SerializedName("status_code")
    var statusCode:Int,
    var success:Boolean

)