package com.juan.testdacodes.mvp.models

import com.google.gson.annotations.SerializedName

data class Country (
        @SerializedName("iso_3166_1")
        var iso31661:String,
        var name:String
) {
}