package com.juan.testdacodes.mvp.models

import com.google.gson.annotations.SerializedName

data class Company (
        var id:Int,
        @SerializedName("logo_path")
        var logoPath:String,
        var name:String,
        @SerializedName("origin_country")
        var originCountry:String
){
}