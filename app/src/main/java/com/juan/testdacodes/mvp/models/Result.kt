package com.juan.testdacodes.mvp.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import retrofit2.SkipCallbackExecutor

data class Result (
    @SerializedName("poster_path")
    var posterPath:String,
    var adult:Boolean,
    var overview:String,
    @SerializedName("release_date")
    var releaseDate:String,
    @SerializedName("genre_ids")
    var genreIds:ArrayList<Int>,
    var id: Int,
    @SerializedName("original_title")
    var originalTitle:String,
    @SerializedName("original_language")
    var originalLanguage:String,
    var title:String,
    @SerializedName("backdrop_path")
    var backdropPath:String,
    var popularity: Float,
    @SerializedName("vote_count")
    var voteCount: Int,
    var video:Boolean,
    @SerializedName("vote_average")
    var voteAverage: Float
)