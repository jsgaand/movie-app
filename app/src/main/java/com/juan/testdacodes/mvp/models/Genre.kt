package com.juan.testdacodes.mvp.models

data class Genre (
        var id:Int,
        var name:String
) {
}