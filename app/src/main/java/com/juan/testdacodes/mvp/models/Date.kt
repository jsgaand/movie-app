package com.juan.testdacodes.mvp.models

data class Date (
    var maximum:String,
    var minimum:String
)