package com.juan.testdacodes.mvp.views.activities

import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.juan.testdacodes.Config
import com.juan.testdacodes.R
import com.juan.testdacodes.helper.Utility
import com.juan.testdacodes.mvp.models.Genre
import com.juan.testdacodes.mvp.models.MovieDetailResponse
import com.juan.testdacodes.rest.TestDaCodesClient
import com.juan.testdacodes.rest.interfaces.TestDaCodesInterfaceJava
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("NonConstantResourceId")
class MovieDetailActivity : AppCompatActivity() {

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar
    @BindView(R.id.iv_image_movie)
    lateinit var ivMovie:ImageView
    @BindView(R.id.tv_title_movie)
    lateinit var tvTitleMovie:TextView
    @BindView(R.id.tv_duration_movie)
    lateinit var tvDurationMovie:TextView
    @BindView(R.id.tv_date_movie)
    lateinit var tvDateReleaseMovie:TextView
    @BindView(R.id.tv_rate_movie)
    lateinit var tvRateMovie:TextView
    @BindView(R.id.tv_genders_movie)
    lateinit var tvGendersMovie:TextView
    @BindView(R.id.tv_description_movie)
    lateinit var tvDescriptionMovie:TextView

    private var movieId:Int?  = 0
    private var callDetails:Call<MovieDetailResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        ButterKnife.bind(this)

        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            movieId = bundle.getInt("movieId",0)
        }

        setSupportActionBar(toolbar)
        val actionBar:ActionBar? = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeButtonEnabled(true)
        }

        getMovieDetails(movieId!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    private fun getMovieDetails(movieId:Int) {
        if (Utility.isNetworkAvailable(this)) {
            val apiKey = Config.API_KEY
            val testDacodesApi: TestDaCodesInterfaceJava = TestDaCodesClient.getClient().create(TestDaCodesInterfaceJava::class.java)
            callDetails = testDacodesApi.getMovieDetails(movieId,apiKey)
            callDetails?.enqueue(object : Callback<MovieDetailResponse>{
                @SuppressLint("SetTextI18n")
                override fun onResponse(@NonNull call: Call<MovieDetailResponse>, @NonNull response: Response<MovieDetailResponse>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val movieDtails: MovieDetailResponse = response.body() as MovieDetailResponse
                            Glide.with(this@MovieDetailActivity).load(Config.BASE_URL_IMAGES + movieDtails.backdropPath).transform(CenterInside(), RoundedCorners(50)).into(ivMovie)
                            tvTitleMovie.text = movieDtails.title
                            tvDateReleaseMovie.text = movieDtails.releaseDate
                            tvDurationMovie.text = movieDtails.runtime.toString() + " min"
                            for (genre in movieDtails.genres) {
                                tvGendersMovie.append(genre.name + " ")
                            }
                            tvRateMovie.text = movieDtails.voteAverage.toString()
                            tvDescriptionMovie.text = movieDtails.overview
                        }
                    } else {
                        when(response.code()) {
                            401,404 -> {
                                val alertDialog:AlertDialog = AlertDialog.Builder(this@MovieDetailActivity).create()
                                alertDialog.setTitle(R.string.app_name)
                                alertDialog.setMessage("Ocurrio una error al obtener la información")
                                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK") { dialog, p1 -> dialog?.dismiss() }
                                alertDialog.show()
                            }
                        }
                    }
                }

                override fun onFailure(@NonNull call: Call<MovieDetailResponse>, @NonNull t: Throwable) {
                    Log.e("onFailure MovieDetails",t.message!!)
                    Toast.makeText(this@MovieDetailActivity,"Error al obtener los detalles de la película",Toast.LENGTH_SHORT).show()
                }
            })

        } else {
            Toast.makeText(this,"No cuentas con conexión a internet",Toast.LENGTH_SHORT).show()
        }
    }
}