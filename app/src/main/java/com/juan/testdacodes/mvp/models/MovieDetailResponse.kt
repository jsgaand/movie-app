package com.juan.testdacodes.mvp.models

import com.google.gson.annotations.SerializedName

data class MovieDetailResponse (
        var adult:Boolean,
        @SerializedName("backdrop_path")
        var backdropPath:String,
        var budget:Int,
        var genres:ArrayList<Genre>,
        var homepage:String,
        var id:Int,
        @SerializedName("imdb_id")
        var imdbID:String,
        @SerializedName("original_language")
        var originalName:String,
        @SerializedName("original_title")
        var originalTitle:String,
        var overview:String,
        var popularity:Float,
        @SerializedName("poster_path")
        var posterPath:String,
        @SerializedName("production_companies")
        var productionCompanies:ArrayList<Company>,
        @SerializedName("production_countries")
        var productionCountries:ArrayList<Country>,
        @SerializedName("release_date")
        var releaseDate:String,
        var revenue:Int,
        var runtime:Int,
        @SerializedName("spoken_languages")
        var spokenLaguajes:ArrayList<Language>,
        var status:String,
        var tagline:String,
        var title:String,
        @SerializedName("vote_average")
        var voteAverage:Float,
        @SerializedName("vote_count")
        var voteCount:Int,

        //Errores
        @SerializedName("status_message")
        var statusMessage:String,
        @SerializedName("status_code")
        var statusCode:Int,
        var success:Boolean





){
}