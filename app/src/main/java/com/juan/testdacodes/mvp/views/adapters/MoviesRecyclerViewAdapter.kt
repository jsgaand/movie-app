package com.juan.testdacodes.mvp.views.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.juan.testdacodes.Config
import com.juan.testdacodes.R
import com.juan.testdacodes.mvp.models.Result

class MoviesRecyclerViewAdapter(var context:Context, listener:MoviesListener):
    RecyclerView.Adapter<MoviesRecyclerViewAdapter.MoviesViewHolder>() {

    private var movies:ArrayList<Result>? = ArrayList()
    private var listener:MoviesListener? = null

    var viewHolder: MoviesViewHolder? = null

    init {
        this.listener = listener
    }

    fun setMovies(movies:ArrayList<Result>) {
        this.movies?.clear()
        this.movies?.addAll(movies)
        notifyDataSetChanged()
    }

    fun addMoreMovies(movies:ArrayList<Result>) {
        this.movies?.addAll(movies)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_movie,parent,false)
        viewHolder = MoviesViewHolder(view,movies!!,listener!!)
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val movie:Result = movies?.get(position) as Result
        holder.tvMovieTitle.text = movie.title
        holder.tvMovieDate.text = movie.releaseDate
        holder.tvRatingMovie.text = movie.voteAverage.toString()
        Glide.with(context).load(Config.BASE_URL_IMAGES + movie.posterPath).transform(CenterInside(),RoundedCorners(30)).into(holder.ivMovie)

    }

    override fun getItemCount(): Int {
        return movies?.size!!
    }

    interface MoviesListener{
        fun onClickMovie(movie:Result)

    }

    @SuppressLint("NonConstantResourceId")
    class MoviesViewHolder(itemView:View, movies: ArrayList<Result>, listener: MoviesListener): RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.iv_movie)
        lateinit var ivMovie: ImageView

        @BindView(R.id.tv_movie_title)
        lateinit var tvMovieTitle: TextView

        @BindView(R.id.tv_movie_date)
        lateinit var tvMovieDate: TextView

        @BindView(R.id.tv_movie_rating)
        lateinit var tvRatingMovie: TextView

        private var movies: ArrayList<Result>? = null
        private var listener: MoviesListener? = null

        init {
            ButterKnife.bind(this, itemView)
            this.movies = movies
            this.listener = listener

        }

        @OnClick(R.id.row_movie)
        fun onClickMovie() {
            listener?.onClickMovie(movies!![adapterPosition])
        }


    }


}