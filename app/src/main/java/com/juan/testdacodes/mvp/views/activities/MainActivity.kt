package com.juan.testdacodes.mvp.views.activities

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindView
import butterknife.ButterKnife
import com.juan.testdacodes.Config
import com.juan.testdacodes.R
import com.juan.testdacodes.helper.Utility
import com.juan.testdacodes.mvp.models.BaseResponse
import com.juan.testdacodes.mvp.models.Result
import com.juan.testdacodes.mvp.views.adapters.MoviesRecyclerViewAdapter
import com.juan.testdacodes.rest.TestDaCodesClient
import com.juan.testdacodes.rest.interfaces.TestDaCodesInterfaceJava
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("NonConstantResourceId")
class MainActivity : AppCompatActivity() , MoviesRecyclerViewAdapter.MoviesListener,
    SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar
    @BindView(R.id.recycler_movies)
    lateinit var recyclerMovies:RecyclerView
    @BindView(R.id.swipeRefresh)
    lateinit var swipeRefresh: SwipeRefreshLayout


    private var callMovies: Call<BaseResponse>? = null
    private var adapter:MoviesRecyclerViewAdapter? = null
    private var layoutManager:GridLayoutManager? = null

    //Pagination Variables
    private var nextPage: Int? = null
    private var previous_total = 0
    private var isLoading = true
    private var view_threshold = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)


        setSupportActionBar(toolbar)

        swipeRefresh.setColorSchemeColors(this.getColor(R.color.colorPrimaryDark))
        swipeRefresh.setOnRefreshListener(this)
        //layoutManager = LinearLayoutManager(this)
        layoutManager = GridLayoutManager(this,2)
        recyclerMovies.setHasFixedSize(true)
        recyclerMovies.layoutManager = layoutManager
        adapter = MoviesRecyclerViewAdapter(this, this)
        recyclerMovies.adapter = adapter

        getMovies()
    }

    private fun getMovies() {
        if (Utility.isNetworkAvailable(this)) {
            val apiKey = Config.API_KEY
            nextPage = 1
            val movieApp:TestDaCodesInterfaceJava = TestDaCodesClient.getClient().create(
                TestDaCodesInterfaceJava::class.java
            )
            callMovies =movieApp.getMovies(apiKey, nextPage!!)
            callMovies?.enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    @NonNull call: Call<BaseResponse>,
                    @NonNull response: Response<BaseResponse>
                ) {
                    if (swipeRefresh.isRefreshing) {
                        swipeRefresh.isRefreshing = false
                    }
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val baseResponse: BaseResponse = response.body() as BaseResponse
                            if (baseResponse.results.size > 0) {
                                adapter?.setMovies(baseResponse.results)
                                nextPage = Integer.valueOf(baseResponse.page) + 1
                            }
                        }
                    } else {
                        when (response.code()) {
                            401, 404 -> {
                                val alertDialog: AlertDialog =
                                    AlertDialog.Builder(this@MainActivity).create()
                                alertDialog.setTitle(R.string.app_name)
                                alertDialog.setMessage("Ocurrio una error al obtener la información")
                                alertDialog.setButton(
                                    DialogInterface.BUTTON_POSITIVE, "OK"
                                ) { dialog, p1 -> dialog?.dismiss() }
                                alertDialog.show()
                            }
                        }
                    }
                }

                override fun onFailure(@NonNull call: Call<BaseResponse>, @NonNull t: Throwable) {
                    Log.e("Call get movies", t.message!!)
                    if (swipeRefresh.isRefreshing) {
                        swipeRefresh.isRefreshing = false
                    }
                    Toast.makeText(this@MainActivity, "Error en la consulta", Toast.LENGTH_SHORT)
                        .show()
                }
            })

            recyclerMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val totalItemCount = layoutManager!!.itemCount
                    val visibleItemCount = layoutManager!!.childCount
                    val firstVisibleItem = layoutManager!!.findFirstVisibleItemPosition()

                    if (dy > 0) {
                        if (isLoading) {
                            if (totalItemCount > previous_total) {
                                isLoading = false
                                previous_total = totalItemCount
                            }
                        }
                        if (!isLoading) {
                            if ((visibleItemCount + firstVisibleItem) >= totalItemCount &&
                                firstVisibleItem >= 0 && totalItemCount >= view_threshold) {
                                performPagination()
                                isLoading = true
                            }
                        }
                    }
                }
            })

        } else {
            Toast.makeText(
                this@MainActivity,
                "No cuentas con conexión a internet",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun performPagination() {
        if (Utility.isNetworkAvailable(this)) {
            val apiKey = Config.API_KEY
            val movieApp:TestDaCodesInterfaceJava = TestDaCodesClient.getClient().create(
                TestDaCodesInterfaceJava::class.java
            )
            callMovies =movieApp.getMovies(apiKey, nextPage!!)
            callMovies?.enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    @NonNull call: Call<BaseResponse>,
                    @NonNull response: Response<BaseResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val baseResponse: BaseResponse = response.body() as BaseResponse
                            if (baseResponse.results.size > 0) {
                                adapter?.addMoreMovies(baseResponse.results)
                                nextPage = Integer.valueOf(baseResponse.page) + 1
                            }
                        }
                    } else {
                        when (response.code()) {
                            401, 404 -> {
                                val alertDialog: AlertDialog =
                                    AlertDialog.Builder(this@MainActivity).create()
                                alertDialog.setTitle(R.string.app_name)
                                alertDialog.setMessage("Message: " + response.body()!!.statusMessage + ", Code: " + response.body()!!.statusCode.toString())
                                alertDialog.setButton(
                                    DialogInterface.BUTTON_POSITIVE, "OK"
                                ) { dialog, p1 -> dialog?.dismiss() }
                                alertDialog.show()
                            }
                        }
                    }
                }

                override fun onFailure(@NonNull call: Call<BaseResponse>, @NonNull t: Throwable) {
                    Log.e("Call get movies more", t.message!!)
                    Toast.makeText(this@MainActivity, "Error en la consulta", Toast.LENGTH_SHORT)
                        .show()
                }
            })

        } else {
            Toast.makeText(
                this@MainActivity,
                "No cuentas con conexión a internet",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onClickMovie(movie: Result) {
        val intent = Intent(this,MovieDetailActivity::class.java)
        intent.putExtra("movieId",movie.id)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)

    }

    override fun onRefresh() {
        getMovies()
        recyclerMovies.scrollToPosition(View.FOCUS_UP)
    }
}