package com.juan.testdacodes.mvp.models

import com.google.gson.annotations.SerializedName

data class Language (
        @SerializedName("iso_639_1")
        var iso6391:String,
        var name:String
) {
}